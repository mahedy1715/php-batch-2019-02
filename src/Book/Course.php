<?php


namespace App;


class Course
{
 private $markBangla,$gradeBangla,$markEnglish,$gradeEnglish,$markMath,$gradeMath;

    public function setMarkBangla($markBangla){
        $this->markBangla=$markBangla;
    }
    public function setGradeBangla($markBangla){
        $this->gradeBangla=$this->convertMark2Grade($markBangla);
    }
    public function setMarkEnglish($markEnglish){
        $this->markEnglish=$markEnglish;
    }
    public function setGradeEnglish($markEnglish){
        $this->gradeEnglish=$this->convertMark2Grade($markEnglish);
    }
    public function setMarkMath($markMath){
        $this->markMath=$markMath;
    }
    public function setGradeMath($markMath){
        $this->gradeMath=$this->convertMark2Grade($markMath);
    }


    /*get*/
    public function getMarkBangla(){
        return $this->markBangla;
    }
    public function getMarkEnglish(){
        return $this->markEnglish;
    }
    public function getMarkMath(){
    return $this->markMath;
}



    public function convertMark2Grade($mark){
        switch($mark){
            case ($mark>79):
                $grade="A+";
                break;
            case ($mark>69):
                $grade="A";
                break;
            case ($mark>59):
                $grade="A-";
                break;
            case ($mark>49):
                $grade="B";
                break;
            case ($mark>39):
                $grade="c";
                break;
            case ($mark>32):
                $grade="d";
                break;
            default:($grade="F");
        }
        return $grade;

    }
}
?>