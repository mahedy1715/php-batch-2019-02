<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <link rel="stylesheet" href="../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../resource/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="../resource/stylesheet/style.css">

    <script src="../resource/bootstrap/js/bootstrap.js"></script>
    <script src="../resource/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>
<div  class="top">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
                <a href="#">
                    <img src="../resource/img/rocketonline.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active"><a class="nav-link" href="#">Home</a></li>
                        <li class="nav-item active"><a class="nav-link" href="#">Service</a></li>
                        <li class="nav-item active"><a class="nav-link" href="#"">About us</a></li>
                        <li class="nav-item active"><a class="nav-link" href="#"">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

</div>